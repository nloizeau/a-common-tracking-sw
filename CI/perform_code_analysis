#!/usr/bin/env bash

# abort on error
set -ex

# check for correct number of arguments
if [ $# -ne 3 ]
then
    echo "wrong number of arguments"
    echo "usage: performa_code_analysis <ACTS_DIR> <BUILD_DIR> <OUTDIR>"
    exit 1
fi

# check for required environment variables
: ${gitlabToken:?"'gitlabToken' not set or empty"}
: ${gitlabTargetNamespace:?"'gitlabTargetNamespace' not set or empty"}
: ${gitlabTargetRepoName:?"'gitlabTargetRepoName' not set or empty"}
: ${gitlabMergeRequestId:?"'gitlabMergeRequestId' not set or empty"}
: ${BUILD_URL:?"'BUILD_URL' not set or empty"}
: ${WORKSPACE:?"'WORKSPACE' not set or empty"}

# set parameters
ACTS_DIR=$1
BUILD_DIR=$2
OUTDIR=$3

# check for ACTS
if [ ! -d "$ACTS_DIR" ]
then
    echo "ACTS_DIR='$ACTS_DIR' not found -> aborting"
    exit 1
fi

# compile with static code analyzer
build_acts $ACTS_DIR --compile-options " -O2" --make-options " | tee build.log" --build-dir $BUILD_DIR --analyze --analyze-out $OUTDIR

# analyze output and publish result
SCA_DIR=`cat $BUILD_DIR/build.log | grep "scan-build: Emitting reports for this" | cut -d "'" -f 2`
SCA_DIR=${SCA_DIR#${WORKSPACE}/}
NBUGS=`cat $BUILD_DIR/build.log | egrep "scan-build: (No|[0-9]+) bugs found" | cut -d " " -f 2`

# generate tarball
tar -czf sca.tar.gz $SCA_DIR

# just to make the comment look better
if [ "$NBUGS" == "No" ]
then
    NBUGS="no"
fi

# add comment about results from static code analysis
COMMENT="static code checker found $NBUGS problems<br />tarball with results can be downloaded from [here]($BUILD_URL/artifact/sca.tar.gz)"
./comment_merge_request add "$COMMENT" --project $gitlabTargetNamespace/$gitlabTargetRepoName --merge-request-id $gitlabMergeRequestId --token $gitlabToken
